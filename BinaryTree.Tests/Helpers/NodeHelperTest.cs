﻿using System.IO;
using System.Text;
using BinaryTree.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinaryTree.Tests.Helpers
{
    [TestClass]
    public class NodeHelperTest
    {
        [TestMethod]
        public void GetNodes_Line_Should_Be_Have_Parent_Left_Rigth()
        {
            // Arrange       
            var expectedCount = 0;
            var stringBuilder = new StringBuilder();
            
            stringBuilder.AppendLine("F,G");

            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));

            // Act
            var result = NodeHelper.GetNodes(streamReader);

            // Assert
            Assert.AreEqual(expectedCount, result.Length);
        }

        [TestMethod]
        public void GetNodes_Parent_Should_Be_Not_Empty()
        {
            // Arrange       
            var expectedCount = 0;
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("#,A,B");

            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));

            // Act
            var result = NodeHelper.GetNodes(streamReader);

            // Assert
            Assert.AreEqual(expectedCount, result.Length);
        }

        [TestMethod]
        public void GetNodes_Parent_Left_Right_Should_Be_Unique_In_Line()
        {
            // Arrange       
            var expectedCount = 0;
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("A,A,B");

            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));

            // Act
            var result = NodeHelper.GetNodes(streamReader);

            // Assert
            Assert.AreEqual(expectedCount, result.Length);
        }

        [TestMethod]
        public void GetNodes_Nodes_Should_Be_Letters()
        {
            // Arrange       
            var expectedCount = 0;
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("1,A,B");

            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));

            // Act
            var result = NodeHelper.GetNodes(streamReader);

            // Assert
            Assert.AreEqual(expectedCount, result.Length);
        }
    }
}

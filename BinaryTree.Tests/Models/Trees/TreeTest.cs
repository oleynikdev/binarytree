﻿using System.IO;
using System.Text;
using BinaryTree.Exceptions;
using BinaryTree.Models.Trees;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinaryTree.Tests.Models.Trees
{
    [TestClass]
    public class TreeTest
    {
        [TestMethod]
        [ExpectedException(typeof (TreeValidateException), "The tree should have one root")]
        public void BuildTree_Should_Be_Tree_Validate_Exception_If_Tree_Have_Many_Roots()
        {

            // Arrange
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("A,B,C");
            stringBuilder.AppendLine("E,F,D");

            var streamReader = new StreamReader(new MemoryStream(Encoding.ASCII.GetBytes(stringBuilder.ToString())));

            // Act

            Tree.BuildTree(streamReader);

            // Assert
          
        }
    }
}

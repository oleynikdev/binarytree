﻿using System;

namespace BinaryTree.Exceptions
{
    [Serializable]
    public class TreeValidateException : Exception
    {
        public TreeValidateException()
        {
        }

        public TreeValidateException(string message)
        : base(message)
        {
        }

        public TreeValidateException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}

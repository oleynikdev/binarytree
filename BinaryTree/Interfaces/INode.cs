﻿using BinaryTree.Models.Trees;

namespace BinaryTree.Interfaces
{
    public interface INode
    {
        string Data { get; set; }

        Node Left { get; set; }
        Node Right { get; set; }
    }
}

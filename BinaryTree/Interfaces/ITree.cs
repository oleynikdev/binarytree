﻿using BinaryTree.Models.Trees;

namespace BinaryTree.Interfaces
{
    public interface ITree
    {
        Node Root { get; }
    }
}

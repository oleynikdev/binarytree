﻿using BinaryTree.Interfaces;

namespace BinaryTree.Models.Trees
{
    public class Node: INode
    {
        public string Data { get; set; }

        public Node Left { get; set; }
        public Node Right { get; set; }
    }
}

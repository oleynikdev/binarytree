﻿using System;
using System.Collections.Generic;
using System.IO;
using BinaryTree.Exceptions;
using BinaryTree.Helpers;
using BinaryTree.Interfaces;

namespace BinaryTree.Models.Trees
{
    public class Tree : ITree
    {
        private Node _root;
        private readonly Queue<Node> _tempRoots;
        private  bool _isPushNode;

        private const string OneRootExMessage = "The tree should have one root";

        public Node Root => _root;

        private Tree()
        {
            _tempRoots = new Queue<Node>();
        }

        public static Tree BuildTree(StreamReader stream)
        {
            var tree = new Tree();
            var nodes = NodeHelper.GetNodes(stream);

            foreach (var node in nodes)
            {
                tree.Insert(node);
            }

            if (tree._tempRoots.Count > 0)
                throw new TreeValidateException(OneRootExMessage);

            return tree;
        }
        
        private void Insert(Node node)
        {
            if (_root == null)
            {
                _root = node;
                return;
            }

            bool isMoveToCurrentRoot = MoveToCurrentRoot(node);
            
            if (isMoveToCurrentRoot && _tempRoots.Count > 0)
                TempMoveToCurrentRoot();
            
            if (!(_isPushNode || isMoveToCurrentRoot))
                _tempRoots.Enqueue(node);
        }
        
        private void TempMoveToCurrentRoot()
        {
            for (int i = 0; i < _tempRoots.Count; i++)
            {
                var tempRoot = _tempRoots.Dequeue();

                var isMoveToCurrentRoot = MoveToCurrentRoot(tempRoot);

                if (isMoveToCurrentRoot)
                {
                    if (_tempRoots.Count > 0)
                        i--;
                }
                else
                {
                    _tempRoots.Enqueue(tempRoot);
                }
            }
        }

        private bool MoveToCurrentRoot(Node node)
        {
            _isPushNode = false;
            
            var res = ChangeCurrentRoot(node);
            
            if (res)
                return true;
            
            PushNode(_root, node);
            
            return _isPushNode;
        }

        private bool ChangeCurrentRoot(Node node)
        {
            bool result = false;

            if (string.Equals(_root.Data, node.Left.Data, StringComparison.Ordinal))
            {
                node.Left = _root;
                _root = node;

                result = true;
            }
            else if (string.Equals(_root.Data, node.Right.Data, StringComparison.Ordinal))
            {
                node.Right = _root;
                _root = node;

                result = true;
            }

            return result;
        }

        private void PushNode(Node root, Node node)
        {
            if (root == null)
                return;

            if (string.Equals(root.Data, node.Data, StringComparison.Ordinal))
            {
                root.Left = node.Left;
                root.Right = node.Right;

                _isPushNode = true;
            }
            else
            {
                PushNode(root.Left, node);
                PushNode(root.Right, node);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BinaryTree.Models.Trees;

namespace BinaryTree.Helpers
{
   public static class NodeHelper
   {
       private const int ElementCount = 3;
       private const string OnlyLettersOrSimRegExpresion = @"^[a-zA-Z]+|#$";
       private const  string ChildEmpty = "#";

       public static Node[] GetNodes(StreamReader stream)
       {
            var nodes = new List<Node>();
            string line;

            while ((line = stream.ReadLine()) != null)
            {   
                string[] arrayStr = line.Split(',')
                                        .Take(3)
                                        .ToArray();

                if (arrayStr.Length < ElementCount)
                    continue;

                if (!arrayStr.All(x => Regex.IsMatch(x, OnlyLettersOrSimRegExpresion)))
                    continue;

                if (arrayStr.Distinct().Count() != arrayStr.Length)
                    continue;

                if (string.Equals(arrayStr[0], ChildEmpty, StringComparison.CurrentCulture))
                    continue;

                nodes.Add(new Node
                {
                    Data = arrayStr[0],

                    Left = new Node
                    {
                        Data = arrayStr[1]
                    },
                    Right = new Node
                    {
                        Data = arrayStr[2]
                    }
                });

            }

            return nodes.ToArray();
        }
    }
}

﻿using System;
using System.IO;
using System.Reflection;
using BinaryTree.Exceptions;
using BinaryTree.Models.Trees;

namespace BinaryTree
{
    class Program
    {
        static void Main(string[] args)
        { 
            string location = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            if (location == null)
                return;

            string path = Path.Combine(location, "data.txt");

            if (!File.Exists(path))
                return;

            FileStream fileStream = null;
            StreamReader streamReader = null;

            try
            {
                fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                streamReader = new StreamReader(fileStream);

                Tree tree = Tree.BuildTree(streamReader);

                Display(tree.Root);
            }
            catch (TreeValidateException ex)
            {

                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                //For example: system exeption ex.Message to log file
                Console.WriteLine("System exeption");
            }
            finally
            {

                fileStream?.Close();
                streamReader?.Close();
            }
        }

        static void Display(Node root)
        {
            if (root != null)
            {
                if (root.Left != null && root.Right != null && root.Data != "#")
                    Console.WriteLine(root.Data + " --> " + root.Left.Data + " -- " + root.Right.Data);

                Display(root.Left);
                Display(root.Right);
            }
        }
    }
}
